<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use \Illuminate\Validation\Rule;

class UsersController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users = User::with('roles')->paginate(10);

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $user  = new User();
        $roles = Role::all();

        return view('users.create', compact('user', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store() {
        $rules = $this->rules(['email' => 'unique:users']);
        $data  = request()->validate($rules);

        $user = User::create($data);
        $this->syncWithRolesValues($user, $data);

        return redirect()->route('users.show', [$user]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user) {
        $roles         = Role::all();
        $user_role_ids = $user->roles->pluck('id')->toArray();

        return view('users.edit', compact('user', 'roles', 'user_role_ids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(User $user) {
        $rules = $this->rules(['email' => Rule::unique('users')->ignore($user)]);
        if (!request()->filled('password')) {
            unset($rules['password']);
        }

        $data = request()->validate($rules);
        $user->update($data);
        $this->syncWithRolesValues($user, $data);

        return redirect()->route('users.show', [$user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        $user->delete();

        return redirect()->route('users.index');
    }
    
    private function syncWithRolesValues($user, $data) {
        $user->roles()->syncWithPivotValues($data['roles'] ?? [], ['role_assigned_by_id' => auth()->user()->id]);
    }

    private function rules($actionRules) {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', $actionRules['email']],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'roles' => 'exists:roles,id',
        ];
    }

}
