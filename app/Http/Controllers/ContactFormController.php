<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller {

    function create() {
        return view('contact.create');
    }

    function store() {

        try {
            $data = request()->validate([
                'name' => 'required',
                'email' => 'required|email',
                'message' => 'required'
            ]);

            Mail::to('test@test.com')->send(new \App\Mail\ContactFormMail($data));

            return redirect('contact')->with('message', 'Thank you for your message.');
        } catch (\Exception $e) {
            return redirect('contact')->with('error', $e->getMessage());
        }
    }

}
