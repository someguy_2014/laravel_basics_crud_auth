<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RoleUser extends Pivot
{
    function role_assigned_by(){
        return $this->belongsTo(User::class, 'role_assigned_by_id');
    }
}
