@extends('layouts.app')

@section('title', __('users.create'))

@section('content')

<div class="row justify-content-center">
    <div class="col-8">
        <h1>{{ __('users.create') }}</h1>        
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-8">
        <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
            @include('users.form')

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('users.create_button') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection