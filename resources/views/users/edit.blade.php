@extends('layouts.app')

@section('title', __('users.update') . ' ' . $user->name)

@section('content')

<div class="row justify-content-center">
    <div class="col-8">
        <h1>{{ __('users.update') . ' ' . $user->name }}</h1>        
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-8">
        <form action="{{ route('users.update', [$user]) }}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
            @include('users.form')

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('users.update_button') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection