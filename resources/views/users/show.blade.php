@extends('layouts.app')

@section('title', __('users.show') . ' ' . $user->name)

@section('content')

<div class="row justify-content-center">
    <div class="col-8">
        <h1>{{ __('users.show') . ' ' . $user->name }}</h1>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-8">
        <div class="row">
            <div class="col-md-4 text-md-right">{{ __('users.name') }}:</div>
            <div class="col-md-6">{{ $user->name }}</div>
        </div>

        <div class="row">
            <div class="col-md-4 text-md-right">{{ __('users.email') }}:</div>
            <div class="col-md-6">{{ $user->email }}</div>
        </div>
    </div>
    <div class="col-8">
        <div class="row">
            <div class="col-md-4 text-md-right">{{ __('users.roles') }}:</div>
            <div class="col-md-6">
                @foreach($user->roles as $role)
                <div class="row">
                    <div class="col-md-12">
                        {{ $role->name }}, created by {{ $role->pivot->role_assigned_by->name }}
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-4 d-flex pt-3">
        <div class="pr-1 w-50">
            <a class="btn btn-primary w-100" href="{{ route('users.edit', [$user]) }}">Edit</a>
        </div>
        <div class="pl-1 w-50">
            <form action="{{ route('users.destroy', [$user]) }}" method="POST">
                @method('DELETE')
                @csrf

                <button class="btn btn-danger w-100" type="submit">Delete</button>
            </form>
        </div>
    </div>
</div>
@endsection