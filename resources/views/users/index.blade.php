@extends('layouts.app')

@section('title', __('users.list'))

@section('content')

<div class="row">
    <div class="col-12">
        <h1>{{ __('users.list') }}</h1>
    </div>
</div>

<div class="row">
    <div class="col-12 py-1">
        <a href="{{ route('users.create') }}">{{ __('users.create') }}</a>
    </div>
</div>

<div class="container-fluid">

    <div class="row">
        <div class="col-1">
            #
        </div>
        <div class="col-3">
            Name
        </div>
        <div class="col-3">
            e-mail
        </div>
        <div class="col-1">
            Roles
        </div>
        <div class="col-2">
            updated at
        </div>
        <div class="col-2">
            created at
        </div>
    </div>
    @foreach($users as $user)
    <div class="row">
        <div class="col-1">
            {{ $user->id }}
        </div>
        <div class="col-3">
            <a href="{{ route('users.show', [$user]) }}">
                {{ $user->name }}
            </a>
        </div>
        <div class="col-3">
            {{ $user->email }}
        </div>
        <div class="col-1">
            @foreach($user->roles as $role)
                {{ $role->name }}
            @endforeach
        </div>
        <div class="col-2">
            {{ $user->updated_at }}
        </div>
        <div class="col-2">
            {{ $user->created_at }}
        </div>
    </div>
    @endforeach

    <div class="row">
        <div class="col-12 d-flex justify-content-center pt-4">
            {{ $users->links() }}
        </div>
    </div>
</div>
@endsection