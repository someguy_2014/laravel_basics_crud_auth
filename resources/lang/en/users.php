<?php

return [
    'list'                  => 'Users list',
    'create'                => 'Add New User',
    'update'                => 'Edit Details for',
    'show'                  => 'Details for',
    'name'                  => 'Name',
    'email'                 => 'E-Mail Address',
    'password'              => 'Password',
    'password_confirmation' => 'Confirm Password',
    'create_button'         => 'Add User',
    'update_button'         => 'Save User',
    'roles'                 => 'Roles',
];
