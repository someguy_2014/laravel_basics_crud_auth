<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('index');
Route::resource('users', App\Http\Controllers\UsersController::class);
Route::resource('customers', App\Http\Controllers\CustomersController::class);
Route::get('contact', [App\Http\Controllers\ContactFormController::class, 'create'])->name('contact.create');
Route::post('contact', [App\Http\Controllers\ContactFormController::class, 'store'])->name('contact.store');
Route::view('about', 'about')->name('about');
