<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        Role::insert([
                ['id' => 1, 'name' => 'admin'],
                ['id' => 2, 'name' => 'technical'],
                ['id' => 3, 'name' => 'marketing'],
            ]
        );
        User::factory()->count(10)->create();

        foreach (User::all() as $user){
            $role_id            = rand(1, 3);
            $role_assigned_by = User::all()->random();

            $user->roles()->attach($role_id, ['role_assigned_by_id' => $role_assigned_by->id ]);
        }

    }

}
